﻿namespace Aplikacja
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.imie = new System.Windows.Forms.TextBox();
            this.nazwisko = new System.Windows.Forms.TextBox();
            this.dodaj = new System.Windows.Forms.Button();
            this.przywitanie = new System.Windows.Forms.Button();
            this.akcja1 = new System.Windows.Forms.Label();
            this.oknowiadomosci = new System.Windows.Forms.Label();
            this.Testowy = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // imie
            // 
            this.imie.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.imie.Location = new System.Drawing.Point(43, 78);
            this.imie.Name = "imie";
            this.imie.Size = new System.Drawing.Size(188, 30);
            this.imie.TabIndex = 0;
            this.imie.Text = "Podaj imie:";
            
            // 
            // nazwisko
            // 
            this.nazwisko.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.nazwisko.Location = new System.Drawing.Point(43, 139);
            this.nazwisko.Name = "nazwisko";
            this.nazwisko.Size = new System.Drawing.Size(188, 30);
            this.nazwisko.TabIndex = 1;
            this.nazwisko.Text = "Podaj Nazwisko:";
            // 
            // dodaj
            // 
            this.dodaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.dodaj.Location = new System.Drawing.Point(254, 107);
            this.dodaj.Name = "dodaj";
            this.dodaj.Size = new System.Drawing.Size(116, 30);
            this.dodaj.TabIndex = 2;
            this.dodaj.Text = "Dodaj";
            this.dodaj.UseVisualStyleBackColor = true;
            // 
            // przywitanie
            // 
            this.przywitanie.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.przywitanie.Location = new System.Drawing.Point(43, 319);
            this.przywitanie.Name = "przywitanie";
            this.przywitanie.Size = new System.Drawing.Size(97, 34);
            this.przywitanie.TabIndex = 3;
            this.przywitanie.Text = "Przyitaj!";
            this.przywitanie.UseVisualStyleBackColor = true;
            this.przywitanie.Click += new System.EventHandler(this.przywitanie_Click);
            // 
            // akcja1
            // 
            this.akcja1.AutoSize = true;
            this.akcja1.Location = new System.Drawing.Point(43, 303);
            this.akcja1.Name = "akcja1";
            this.akcja1.Size = new System.Drawing.Size(0, 13);
            this.akcja1.TabIndex = 4;
            // 
            // oknowiadomosci
            // 
            this.oknowiadomosci.AutoSize = true;
            this.oknowiadomosci.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.oknowiadomosci.Location = new System.Drawing.Point(43, 429);
            this.oknowiadomosci.Name = "oknowiadomosci";
            this.oknowiadomosci.Size = new System.Drawing.Size(42, 17);
            this.oknowiadomosci.TabIndex = 5;
            this.oknowiadomosci.Text = "Akcja";
            // 
            // Testowy
            // 
            this.Testowy.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.Testowy.Location = new System.Drawing.Point(43, 246);
            this.Testowy.Name = "Testowy";
            this.Testowy.Size = new System.Drawing.Size(97, 34);
            this.Testowy.TabIndex = 6;
            this.Testowy.Text = "Test";
            this.Testowy.UseVisualStyleBackColor = true;
            this.Testowy.Click += new System.EventHandler(this.Testowy_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 503);
            this.Controls.Add(this.Testowy);
            this.Controls.Add(this.oknowiadomosci);
            this.Controls.Add(this.akcja1);
            this.Controls.Add(this.przywitanie);
            this.Controls.Add(this.dodaj);
            this.Controls.Add(this.nazwisko);
            this.Controls.Add(this.imie);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox imie;
        private System.Windows.Forms.TextBox nazwisko;
        private System.Windows.Forms.Button dodaj;
        private System.Windows.Forms.Button przywitanie;
        private System.Windows.Forms.Label akcja1;
        public System.Windows.Forms.Label oknowiadomosci;
        private System.Windows.Forms.Button Testowy;
    }
}

